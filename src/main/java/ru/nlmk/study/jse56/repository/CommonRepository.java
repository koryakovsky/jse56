package ru.nlmk.study.jse56.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.nlmk.study.jse56.config.HibernateConfig;
import ru.nlmk.study.jse56.model.Car;

import java.io.Serializable;
import java.util.List;

public class CommonRepository {

    private SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> void save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
    }

    public <T> T getById(Class<T> entityClass, Serializable id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        T entity = session.get(entityClass, id);

        tx.commit();
        session.close();

        return entity;
    }

    // JPQL
    public  List<Car> getAllCars() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<Car> cars = session.createQuery("SELECT c from Car c", Car.class).getResultList();

        tx.commit();
        session.close();

        return cars;
    }
}
