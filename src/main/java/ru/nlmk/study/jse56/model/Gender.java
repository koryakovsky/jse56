package ru.nlmk.study.jse56.model;

public enum Gender {

    FEMALE,
    MALE
}
