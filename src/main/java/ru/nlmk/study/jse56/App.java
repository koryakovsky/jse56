package ru.nlmk.study.jse56;

import ru.nlmk.study.jse56.model.Car;
import ru.nlmk.study.jse56.repository.CommonRepository;

import java.util.List;

public class App {

    public static void main(String[] args) {
        CommonRepository repository = new CommonRepository();

//        Customer customer = new Customer();
//        customer.setPhone("89997776655");
//        customer.setEmail("tomas@mail.com");
//        customer.setRegDate(LocalDateTime.now());
//
//        Passport passport = new Passport();
//        passport.setGender(Gender.MALE);
//        passport.setFirstName("Tomas");
//        passport.setLastName("Edisson");
//        passport.setBirthDate(LocalDate.of(1986, 7, 5));
//        passport.setSeries(1111);
//        passport.setNumber(222222);
//
//        customer.setPassport(passport);
//        passport.setCustomer(customer);

//        Customer savedCustomer = repository.getById(Customer.class, 2);
//        Passport savedPassport = repository.getById(Passport.class, 2);

//        Customer customer = repository.getById(Customer.class, 2);
//
//        List<Car> cars = customer.getCars();

        List<Car> cars = repository.getAllCars();

        System.out.println(cars);

//        Car car1 = new Car();
//        car1.setVin("7687JHJH");
//        car1.setModel("Alfa Romeo");
//        car1.setOwner(customer);
//
//        Car car2 = new Car();
//        car2.setVin("989767JHJH");
//        car2.setModel("Kia");
//        car2.setOwner(customer);

//        repository.save(car1);
//        repository.save(car2);
    }
}
